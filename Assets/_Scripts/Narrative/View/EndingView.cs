﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public struct EndingPicture
{
    public int Id;
    public GameObject EndingObject;
}

public class EndingView : MonoBehaviour {

    public Image OverlayImage;
    public Text EndingText;
    public List<EndingPicture> PictureList;
    public List<GameObject> Arrow;

    private NarrativeEnding currentEnding;
    private bool canSkip;
    private bool completedText;
    private string endingString;
    private Coroutine endingCoroutine;

    private float endingTicks;
    private float endingSeconds;

    private void FixedUpdate()
    {
        endingTicks += Time.fixedDeltaTime;
        endingSeconds = endingTicks % 60;
    }

    public void StartEnding(NarrativeEnding ending)
    {
        currentEnding = ending;

        GooglePlayManager.Instance.UnlockEnding(currentEnding.EndingId);

        if (ending.IsGoodEnding)
        {
            SoundManager.Instance.PlaySFX("GoodEnding");
            SoundManager.Instance.PlayBGM("GoodEnding");
        }
        else
        {
            SoundManager.Instance.PlaySFX("BadEnding");
            SoundManager.Instance.PlayBGM("BadEnding");
        }

        SoundManager.Instance.AmbienceSource.Stop();

        GameObject endingObject = PictureList.Find(p => p.Id == ending.EndingPicture).EndingObject;

        EndingText = endingObject.GetComponentInChildren<Text>();

        OverlayImage.gameObject.SetActive(true);
        StartCoroutine(BackgroundAnimation(1f, OverlayImage, 2f, 0f));
        StartCoroutine(DelayActivateObject(endingObject, 2f));
        StartCoroutine(BackgroundAnimation(0f, OverlayImage, 2f, 2f));

        completedText = false;
        endingString = ending.EndingTitle;
        endingCoroutine = StartCoroutine(StaticMethods.TextWriter(EndingText, ending.EndingTitle, () => {
            completedText = true;
            for (int i = 0; i < Arrow.Count; i++)
            {
                Arrow[i].SetActive(true);
            }
        }, 3f));
        StartCoroutine(StartCounterToSkip());

        //Analytics timer
        endingTicks = 0;
    }

    private void SendEndingAnalytics(NarrativeEnding ending)
    {
        AnalyticsEvent.Custom(GameConstants.ENDING_ANALYTICS_KEY, new Dictionary<string, object>
        {
            { GameConstants.CURRENT_ENDING_ID, ending.EndingId},
            { GameConstants.TIME_TO_SKIP_ENDING, endingSeconds}
        });
    }

    IEnumerator DelayActivateObject(GameObject obj, float delay)
    {
        yield return new WaitForSeconds(delay);
        obj.SetActive(true);
    }

    IEnumerator StartCounterToSkip()
    {
        yield return new WaitForSeconds(3f);
        canSkip = true;
    }

    IEnumerator BackgroundAnimation(float newAlpha, Image image, float time, float delay)
    {
        yield return new WaitForSeconds(delay);

        image.gameObject.SetActive(true);

        float elapsedTime = 0;
        float prevAlpha = image.color.a;

        while (elapsedTime < time)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, Mathf.Lerp(prevAlpha, newAlpha, (elapsedTime / time)));
            elapsedTime += Time.fixedDeltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    public void ClickSkip()
    {
        if (completedText)
        {
            SendEndingAnalytics(currentEnding);
            SceneManager.LoadScene("Intro");

            // Show an ad:
            Advertisement.Show();
        }

        if (canSkip)
        {
            completedText = true;
            EndingText.text = endingString;
            StopCoroutine(endingCoroutine);
            for (int i = 0; i < Arrow.Count; i++)
            {
                Arrow[i].SetActive(true);
            }
        }
    }
}
 