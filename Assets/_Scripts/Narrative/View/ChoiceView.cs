﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceView : MonoBehaviour {

    public Text ChoiceText;
    private int choiceId;
    public int ChoiceId
    {
        get { return choiceId; }
    }
    private NarrativeChoice narrativeChoice;

    public void Setup(NarrativeChoice choice)
    {
        narrativeChoice = choice;
        ChoiceText.text = narrativeChoice.ChoiceTitle;
    }

	public void OnClick()
    {
        SoundManager.Instance.PlaySFX("Choice");
        NarrativeCore.Instance.SubmitDialogAnswer(narrativeChoice);
    }
}
