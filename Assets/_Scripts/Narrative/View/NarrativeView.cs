﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NarrativeView : MonoBehaviour {

    public GameObject DialogBox;
    public GameObject AnswerBox;
    public GameObject ChoicesBox;
    public GameObject QuestionTouchTrigger;
    public GameObject AnswerTouchTrigger;
    public GameObject SkipArrow;
    public List<ChoiceView> ChoiceList;

    private NarrativeChoice answeredChoice;
    private NarrativeDialog currentDialog;
    private Coroutine textCoroutine;
    private bool completedAnswerText;

    public void SetupQuestion(NarrativeDialog dialog)
    {
        currentDialog = dialog;

        AnswerBox.gameObject.SetActive(false);
        DialogBox.gameObject.SetActive(true);

        StartCoroutine(DelayActivateObject(QuestionTouchTrigger, 0.5f));

        textCoroutine = StartCoroutine(StaticMethods.TextWriter(DialogBox.GetComponentInChildren<Text>(), currentDialog.DialogTitle, () => {
            SetChoices();
        },0, true, true));
    }

    public void SetChoices()
    {
        ChoicesBox.SetActive(true);
        StopCoroutine(textCoroutine);
        QuestionTouchTrigger.SetActive(false);
        DialogBox.GetComponentInChildren<Text>().text = currentDialog.DialogTitle;

        for (int i = 0; i < currentDialog.DialogChoices.Count; i++)
        {
            ChoiceList[i].GetComponent<ChoiceView>().Setup(currentDialog.DialogChoices[i]);
        }
    }

    public void SetupAnswer(NarrativeChoice choice)
    {
        DialogBox.GetComponentInChildren<Text>().text = "";

        StartCoroutine(DelayActivateObject(AnswerTouchTrigger, 2.5f));
        StartCoroutine(DelayActivateObject(SkipArrow, 2.5f));

        answeredChoice = choice;

        completedAnswerText = false;

        AnswerBox.gameObject.SetActive(true);
        DialogBox.gameObject.SetActive(false);
        ChoicesBox.gameObject.SetActive(false);
        textCoroutine = StartCoroutine(StaticMethods.TextWriter(AnswerBox.GetComponentInChildren<Text>(), answeredChoice.ChoiceSpeak, () => {
            completedAnswerText = true;
        }, 0, false, true));
    }

    IEnumerator DelayActivateObject(GameObject obj, float delay)
    {
        yield return new WaitForSeconds(delay);
        obj.SetActive(true);
    }

    public void SkipAnswer()
    {
        if(completedAnswerText)
        {
            AnswerTouchTrigger.SetActive(false);
            SkipArrow.SetActive(false);
            completedAnswerText = false;
            NarrativeCore.Instance.CheckForEndgame();
        }

        if (textCoroutine != null)
            StopCoroutine(textCoroutine);

        AnswerBox.GetComponentInChildren<Text>().text = answeredChoice.ChoiceSpeak;

        completedAnswerText = true;
    }
}
