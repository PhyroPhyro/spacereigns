﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct CharacterModal
{
    public int Id;
    public Animator Animator;
}

public class CharacterView : MonoBehaviour {

    public List<CharacterModal> CharacterList = new List<CharacterModal>();

    public Animator CharacterAnimator;

    public void SetupCharacter(NarrativeDialog dialog)
    {
        CharacterModal modal = CharacterList.Find(c => c.Id == dialog.DialogCharacterId);

        if(CharacterAnimator != null)
            CharacterAnimator.gameObject.SetActive(false);

        CharacterAnimator = modal.Animator;
        CharacterAnimator.gameObject.SetActive(true);
    }

    public void SetCharacterAnimation(string animName)
    {
        CharacterAnimator.SetTrigger(animName);
    }
}
