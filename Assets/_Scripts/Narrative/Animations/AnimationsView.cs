﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public struct NarrativeAnimation
{
    public int AnimationId;
    public PlayableAsset AnimationTimeline;
    public string TriggerEnterAnimation;
    public string TriggerExitAnimation;
}

public class AnimationsView : MonoBehaviour {

    public CharacterView CharacterView;
    public PlayableDirector PlayableDirector;
    public List<NarrativeAnimation> AnimationList;

    public void SetupDialogAnimation(NarrativeDialog dialog)
    {
        CharacterView.SetupCharacter(dialog);
        SoundManager.Instance.PlaySFX("Door");
        NarrativeAnimation animationItem = AnimationList.Find(a => a.AnimationId == dialog.DialogEnterAnimationId);
        CharacterView.SetCharacterAnimation(animationItem.TriggerEnterAnimation);
        PlayTimeline(animationItem.AnimationTimeline);
    }

    public void SetupIdleAnimation()
    {
        CharacterView.SetCharacterAnimation("Idle");
    }

    public void SetupChoiceAnimation(NarrativeChoice choice)
    {
        NarrativeAnimation animationItem = AnimationList.Find(a => a.AnimationId == choice.ChoiceEndingAnimation);
        //StartCoroutine(StaticMethods.ExecuteDelayed(() => { CharacterView.SetCharacterAnimation(animationItem.TriggerExitAnimation); }, 2f));
        PlayTimeline(animationItem.AnimationTimeline);
    }

    public void PlayTimeline(PlayableAsset playable)
    {
        PlayableDirector.Play(playable);
    }
}
