﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeEnding {

    public int EndingId;
    public int EndingTurns;
    public int EndingPicture;
    public string EndingTitle;
    public bool IsGoodEnding;
    public List<NarrativeChoice> EndingNecessaryChoices;
    public Dictionary<NarrativeTrait, int> EndingNecessaryTraits;
}
