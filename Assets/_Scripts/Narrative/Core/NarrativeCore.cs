﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class NarrativeCore : Singleton<NarrativeCore>
{ 
    public Dictionary<int,NarrativeTrait> CoreTraits;
    public Dictionary<int,NarrativeDialog> CoreDialog;
    public Dictionary<int,NarrativeChoice> CoreChoices;
    public Dictionary<int,NarrativeEnding> CoreEndings;

    public NarrativeView NarrativeView;
    public AnimationsView AnimationsView;
    public EnergyBarView EnergyBarView;
    public EndingView EndingView;
    public int TraitThreshold;
    public float EnergyOvertime;
    public bool IsPaused;

    private List<int> pastDialogsIds;
    private List<int> pastChoicesIds;
    private NarrativeDialog currentDialog;

    private Dictionary<NarrativeEnding, int> narrativeEndingCount = new Dictionary<NarrativeEnding, int>();
    private int dialogLoopCount;

    private float dialogAnswerTicks;
    private float dialogAnswerSeconds;

    private void Awake()
    {
        pastDialogsIds = new List<int>();
        pastChoicesIds = new List<int>();
    }

    private void FixedUpdate()
    {
        dialogAnswerTicks += Time.fixedDeltaTime;
        dialogAnswerSeconds = dialogAnswerTicks % 60;
    }

    public void StartNarrative()
    {
        ReducingEnergy();
        SetupNextDialog();

        //DEBUG
        foreach (var entry in CoreTraits)
        {
            Debug.Log(entry.Value.TraitName + "/" + entry.Value.TraitValue);
        }
    }

    public void ReducingEnergy()
    {
        if(!IsPaused)
        {
            CoreTraits[1].TraitValue -= EnergyOvertime;
            if (CoreTraits[1].TraitValue < 0)
                CoreTraits[1].TraitValue = 0;

            EnergyBarView.SetNewPercentage(CoreTraits[1].TraitValue / 100, false, ReducingEnergy);
        }
        /*DEBUG
        CoreTraits[1].TraitValue = 20;
        CoreTraits[2].TraitValue = 20;
        CoreTraits[3].TraitValue = 70;
        */
    }

    public void CheckForEndgame()
    {
        List<NarrativeEnding> possibleEndings = new List<NarrativeEnding>();

        foreach (var endingEntry in CoreEndings)
        {
            bool isValidEntry = true;

            foreach (var traitEntry in endingEntry.Value.EndingNecessaryTraits)
            {
                if (endingEntry.Value.EndingNecessaryTraits.Count == 1)
                {
                    if(isValidEntry)
                    {
                        if ((traitEntry.Value == CoreTraits[traitEntry.Key.TraitId].TraitValue))
                        {
                            if (!possibleEndings.Contains(endingEntry.Value))
                                possibleEndings.Add(endingEntry.Value);
                        }
                        else
                        {
                            if (possibleEndings.Contains(endingEntry.Value))
                                possibleEndings.Remove(endingEntry.Value);

                            isValidEntry = false;
                        }
                    }
                }
                else
                {
                    if(isValidEntry)
                    {
                        if ((traitEntry.Value >= CoreTraits[traitEntry.Key.TraitId].TraitValue - TraitThreshold &&
                            traitEntry.Value <= CoreTraits[traitEntry.Key.TraitId].TraitValue + TraitThreshold))
                        {
                            if (!possibleEndings.Contains(endingEntry.Value))
                                possibleEndings.Add(endingEntry.Value);
                        }
                        else
                        {
                            if (possibleEndings.Contains(endingEntry.Value))
                                possibleEndings.Remove(endingEntry.Value);

                            isValidEntry = false;
                        }
                    }
                }
            }
            foreach (var choiceEntry in endingEntry.Value.EndingNecessaryChoices)
            {
                if (isValidEntry)
                {
                    if (pastChoicesIds.Contains(choiceEntry.ChoiceId))
                    {
                        if (!possibleEndings.Contains(endingEntry.Value))
                            possibleEndings.Add(endingEntry.Value);
                    }
                    else
                    {
                        if (possibleEndings.Contains(endingEntry.Value))
                            possibleEndings.Remove(endingEntry.Value);

                        isValidEntry = false;
                    }
                }
            }

            if (endingEntry.Value.EndingNecessaryChoices.Count <= 0 && endingEntry.Value.EndingNecessaryTraits.Count <= 0)
                possibleEndings.Add(endingEntry.Value);
        }

        if(possibleEndings.Count > 0)
        {
            List<NarrativeEnding> finalEndings = new List<NarrativeEnding>();

            foreach (var endingEntry in possibleEndings)
            {
                if ((endingEntry.EndingNecessaryChoices.Count <= 0 && endingEntry.EndingNecessaryTraits.Count <= 0) ||
                    (endingEntry.EndingNecessaryTraits.Count == 1 && endingEntry.EndingNecessaryTraits.ContainsKey(CoreTraits[1])))
                {
                    if (pastDialogsIds.Count >= endingEntry.EndingTurns)
                    {
                        EndingView.StartEnding(endingEntry);
                        return;
                    }
                    continue;
                }

                if (narrativeEndingCount.ContainsKey(endingEntry))
                {
                    narrativeEndingCount[endingEntry]++;
                    if (endingEntry.EndingTurns <= narrativeEndingCount[endingEntry])
                        finalEndings.Add(endingEntry);
                }
                else
                {
                    narrativeEndingCount.Add(endingEntry, 1);
                }
            }

            List<NarrativeEnding> removingEndings = new List<NarrativeEnding>();
            foreach (var lastEntry in narrativeEndingCount.Keys)
            {
                if (!possibleEndings.Contains(lastEntry))
                    removingEndings.Add(lastEntry);
            }
            for (int i = 0; i < removingEndings.Count; i++)
            {
                narrativeEndingCount.Remove(removingEndings[i]);
            }

            if (finalEndings.Count > 0)
            {
                NarrativeEnding ending = finalEndings[UnityEngine.Random.Range(0, finalEndings.Count)];
                while(finalEndings.Count > 0 && ending.EndingTurns > narrativeEndingCount[ending])
                {
                    finalEndings.Remove(ending);
                    ending = finalEndings[UnityEngine.Random.Range(0, finalEndings.Count)];
                }

                if (finalEndings.Count > 0)
                    EndingView.StartEnding(ending);
                else
                    SetupNextDialog();
            }
            else
            {
                SetupNextDialog();
            }
        }
        else
        {
            SetupNextDialog();
        }

        NarrativeEnding currentEnding = possibleEndings[UnityEngine.Random.Range(0, possibleEndings.Count)];
    }

    public void SetupNextDialog()
    {
        //Reset analytcs timer
        dialogAnswerTicks = 0;

        List<NarrativeDialog> possibleDialogs = new List<NarrativeDialog>();
        foreach (var diagEntry in CoreDialog)
        {
            bool isValidDialog = true;
            foreach (var traitEntry in diagEntry.Value.DialogNecessaryTraits)
            {
                if(isValidDialog)
                {
                    if (traitEntry.Value > CoreTraits[traitEntry.Key.TraitId].TraitValue - TraitThreshold &&
                        traitEntry.Value < CoreTraits[traitEntry.Key.TraitId].TraitValue + TraitThreshold)
                    {
                        if (!possibleDialogs.Contains(diagEntry.Value))
                            possibleDialogs.Add(diagEntry.Value);
                    }
                    else
                    {
                        if (possibleDialogs.Contains(diagEntry.Value))
                            possibleDialogs.Remove(diagEntry.Value);

                        isValidDialog = false;
                    }
                }
            }
        }

        NarrativeDialog nextDialog;

        if (possibleDialogs.Count > 0 && dialogLoopCount < 3)
        {
            nextDialog = possibleDialogs[UnityEngine.Random.Range(0, possibleDialogs.Count)];
        }
        else
        {
            nextDialog = CoreDialog[UnityEngine.Random.Range(1, CoreDialog.Keys.Count + 1)];
        }

        //CHECKS
        bool resetNextDialog = false;
        if (pastDialogsIds.Contains(nextDialog.DialogId))
            resetNextDialog = true;
        for (int i = 0; i < nextDialog.DialogNecessaryChoices.Count; i++)
        {
            if (!pastChoicesIds.Contains(nextDialog.DialogNecessaryChoices[i].ChoiceId))
                resetNextDialog = true;
        }

        if(resetNextDialog)
        {
            bool hasAnyPossible = false;
            for (int i = 0; i < possibleDialogs.Count; i++)
            {
                if (!pastDialogsIds.Contains(possibleDialogs[i].DialogId))
                    hasAnyPossible = true;
            }

            if (hasAnyPossible)
            {
                dialogLoopCount++;
                SetupNextDialog();
                return;
            }
            else
            {
                List<NarrativeDialog> nonNecessitiesDialogs = new List<NarrativeDialog>();
                foreach(var dialog in CoreDialog)
                {
                    if (dialog.Value.DialogNecessaryChoices.Count <= 0 && !pastDialogsIds.Contains(dialog.Value.DialogId))
                        nonNecessitiesDialogs.Add(dialog.Value);
                }
                nextDialog = nonNecessitiesDialogs[UnityEngine.Random.Range(0, nonNecessitiesDialogs.Count)];
            }
        }

        dialogLoopCount = 0;
        currentDialog = nextDialog;
        pastDialogsIds.Add(nextDialog.DialogId);

        AnimationsView.SetupDialogAnimation(nextDialog);
    }

    public void StartCurrentDialog()
    {
        NarrativeView.SetupQuestion(currentDialog);
        AnimationsView.SetupIdleAnimation();
    }

    public void SubmitDialogAnswer(NarrativeChoice narrativeChoice)
    {
        SoundManager.Instance.PlaySpeech = false;

        foreach (var entry in narrativeChoice.ChoiceTraitModifiers)
        {
            float previousValue = CoreTraits[entry.Key.TraitId].TraitValue;

            CoreTraits[entry.Key.TraitId].TraitValue += entry.Value;
            if (CoreTraits[entry.Key.TraitId].TraitValue > 100)
                CoreTraits[entry.Key.TraitId].TraitValue = 100;
            else if (CoreTraits[entry.Key.TraitId].TraitValue < 0)
                CoreTraits[entry.Key.TraitId].TraitValue = 0;

            if (CoreTraits[entry.Key.TraitId].TraitName == "Energy" &&
                previousValue != CoreTraits[entry.Key.TraitId].TraitValue)
            {
                EnergyBarView.SetNewPercentage((float)CoreTraits[entry.Key.TraitId].TraitValue / 100, true);
            }
        }

        //DEBUG
        foreach (var entry in CoreTraits)
        {
            Debug.Log(entry.Value.TraitName + "/" + entry.Value.TraitValue);
        }

        pastChoicesIds.Add(narrativeChoice.ChoiceId);
        StartChoiceAnswer(narrativeChoice);
        SendDialogAnalytics(narrativeChoice);
    }

    private void SendDialogAnalytics(NarrativeChoice choice)
    {
        AnalyticsEvent.Custom(GameConstants.ANSWERED_DIALOG_ANALYTICS_KEY, new Dictionary<string, object>
        {
            { GameConstants.CURRENT_DIALOG_ID, currentDialog.DialogId},
            { GameConstants.CURRENT_CHOICE_ID, choice.ChoiceId},
            { GameConstants.CURRENT_DIALOG_TIME_TO_ANSWER, dialogAnswerSeconds}
        });
    }

    private void StartChoiceAnswer(NarrativeChoice choice)
    {
        NarrativeView.SetupAnswer(choice);
        AnimationsView.SetupChoiceAnimation(choice);
    }
}
