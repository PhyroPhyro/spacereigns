﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeTrait
{
    public int TraitId;
    public string TraitName;
    public float TraitValue;
}
