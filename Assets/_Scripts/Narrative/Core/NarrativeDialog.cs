﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeDialog
{
    public int DialogId;
    public string DialogTitle;
    public int DialogCharacterId;
    public int DialogEnterAnimationId;
    public List<NarrativeChoice> DialogChoices;
    public List<NarrativeChoice> DialogNecessaryChoices;
    public Dictionary<NarrativeTrait, int> DialogNecessaryTraits;
}
