﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeChoice
{
    public int ChoiceId;
    public string ChoiceTitle;
    public string ChoiceSpeak;
    public int ChoiceEndingAnimation;
    public Dictionary<NarrativeTrait, int> ChoiceTraitModifiers;
}
