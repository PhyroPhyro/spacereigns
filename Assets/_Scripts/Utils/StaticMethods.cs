﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class StaticMethods {

    public static IEnumerator ExecuteDelayed(Action callback, float delay)
    {
        yield return new WaitForSeconds(delay);

        callback();
    }

    public static IEnumerator TextWriter(Text textField, string textString, Action textReturn, float delay = 0f, bool isQuestion = false, bool playSpeech = false)
    {
        SoundManager.Instance.PlaySpeech = true;

        yield return new WaitForSeconds(delay);

        List<char> textSplit = new List<char> (textString.ToCharArray());
        textField.text = "";
        string nextString = "";
        while(textSplit.Count > 0)
        {
            nextString = nextString + textSplit[0].ToString();
            textField.text = nextString;
            textSplit.RemoveAt(0);

            if(playSpeech && SoundManager.Instance.PlaySpeech)
            {
                if (isQuestion)
                    SoundManager.Instance.PlayQuestion("Speech");
                else
                    SoundManager.Instance.PlayAnswer("Speech");
            }

            yield return new WaitForSecondsRealtime(0.02f);
        }

        if(textReturn != null)
            textReturn();
    }
}
