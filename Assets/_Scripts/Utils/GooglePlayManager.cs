﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.OurUtils;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class GooglePlayManager : Singleton<GooglePlayManager>
{
    public GameObject PlayGamesPopup;

    void Start()
    {
#if UNITY_ANDROID
        TryToInitPlayGames(null);
    #endif
    }

    public void TryToInitPlayGames(Action callback)
    {
        if (PlatformUtils.Supported)
        {
            InitPlayGames(callback);
        }
        else
        {
            PlayGamesPopup.SetActive(true);
        }
    }

    public void DownloadPlayGames()
    {
        Application.OpenURL("market://details?id=com.google.android.play.games");
    }

    private void InitPlayGames(Action callback)
    {
        // Activate the Google Play Games platform
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .RequestIdToken()
            .Build();

        PlayGamesPlatform.InitializeInstance(config);

        PlayGamesPlatform.DebugLogEnabled = Debug.isDebugBuild;
        PlayGamesPlatform.Activate();

        // authenticate user:
        PlayGamesPlatform.Instance.Authenticate((success) => {
            if (success && callback != null)
                callback();
        });
    }

    public void UnlockEnding(int endingId)
    {
        switch(endingId)
        {
            case 1:
                ReportAchievement(GPGSIds.achievement_lab_virus);
                break;
            case 2:
                ReportAchievement(GPGSIds.achievement_neglected_mechanical);
                break;
            case 3:
                ReportAchievement(GPGSIds.achievement_enemies_in_sight);
                break;
            case 4:
                ReportAchievement(GPGSIds.achievement_dictatorship);
                break;
            case 5:
                ReportAchievement(GPGSIds.achievement_rioting);
                break;
            case 6:
                ReportAchievement(GPGSIds.achievement_lights_out);
                break;
            case 7:
                ReportAchievement(GPGSIds.achievement_damaging_riot);
                break;
            case 8:
                ReportAchievement(GPGSIds.achievement_hopeless_crew);
                break;
            case 9:
                ReportAchievement(GPGSIds.achievement_spoiled_humans);
                break;
            case 10:
                ReportAchievement(GPGSIds.achievement_planet_bob);
                break;
        }
    }

    public void ReportAchievement(string achievementId)
    {
        PlayGamesPlatform.Instance.ReportProgress(achievementId, 100.0f, (bool success) => {
            // handle success or failure
        });
    }
}