﻿public static class GameConstants
{
    public static string GOOGLE_PLAY_UNITY_ADS = "3254694";
    public static string APPLE_STORE_UNITY_ADS = "3254695";
    public static bool ADS_TEST_MODE = true;

    public static string OPENING_ANALYTICS_KEY = "game_opening";
    public static string TIME_TO_SKIP_OPENING = "opening_time";
    public static string START_GAME_COUNT = "start_game_count";

    public static string ANSWERED_DIALOG_ANALYTICS_KEY = "answered_dialog";
    public static string CURRENT_DIALOG_ID = "current_dialog";
    public static string CURRENT_CHOICE_ID = "chosen_choice";
    public static string CURRENT_DIALOG_TIME_TO_ANSWER = "current_dialog_time";

    public static string ENDING_ANALYTICS_KEY = "game_ending";
    public static string CURRENT_ENDING_ID = "current_ending";
    public static string TIME_TO_SKIP_ENDING = "ending_time";
}
