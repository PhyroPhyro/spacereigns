﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnTime : MonoBehaviour {

    public float timeToDestroy;

	void Update () {
        timeToDestroy -= Time.deltaTime;

        if (timeToDestroy <= 0)
            DestroyImmediate(gameObject);
	}
}
