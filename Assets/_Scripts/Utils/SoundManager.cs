﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SoundSFX
{
    public string SoundName;
    public AudioClip SoundClip;
}

public class SoundManager : Singleton<SoundManager> {

    public List<SoundSFX> SoundList;
    public List<SoundSFX> MusicList;
    public List<SoundSFX> AmbienceList;
    public AudioSource SfxSource;
    public AudioSource BgmSource;
    public AudioSource AmbienceSource;
    public AudioSource QuestionSource;
    public AudioSource AnswerSource;

    public bool IsMuted;
    public bool PlaySpeech = true;

    public void PlaySFX(string sfxName)
    {
        SfxSource.Stop();
        SfxSource.PlayOneShot(SoundList.Find(s => s.SoundName == sfxName).SoundClip);
        SfxSource.loop = false;
    }

    public void PlayBGM(string bgmName)
    {
        BgmSource.Stop();
        BgmSource.clip = MusicList.Find(s => s.SoundName == bgmName).SoundClip;
        BgmSource.loop = true;
        BgmSource.Play();
    }

    public void PlayAmbience(string bgmName)
    {
        AmbienceSource.Stop();
        AmbienceSource.clip = AmbienceList.Find(s => s.SoundName == bgmName).SoundClip;
        AmbienceSource.loop = true;
        AmbienceSource.Play();
    }

    public void PlayQuestion(string sfxName)
    {
        QuestionSource.PlayOneShot(SoundList.Find(s => s.SoundName == sfxName).SoundClip);
        QuestionSource.loop = false;
    }

    public void PlayAnswer(string sfxName)
    {
        AnswerSource.PlayOneShot(SoundList.Find(s => s.SoundName == sfxName).SoundClip);
        AnswerSource.loop = false;
    }

    public void MuteGame()
    {
        IsMuted = !IsMuted;
        if(IsMuted)
        {
            SfxSource.volume = 0;
            BgmSource.volume = 0;
            AmbienceSource.volume = 0;
        }
        else
        {
            SfxSource.volume = 0.7f;
            BgmSource.volume = 0.5f;
            AmbienceSource.volume = 0.3f;
        }
    }
}
