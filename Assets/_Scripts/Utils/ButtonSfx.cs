﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSfx : MonoBehaviour {

    public string SFXName;
    
	public void PlaySFX()
    {
        SoundManager.Instance.PlaySFX(SFXName);
    }
}
