﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayNextDialogOnAwake : MonoBehaviour {

    private void OnEnable()
    {
        NarrativeCore.Instance.StartCurrentDialog();
    }
}
