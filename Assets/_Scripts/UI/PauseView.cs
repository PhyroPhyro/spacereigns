﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseView : MonoBehaviour {

    public GameObject CreditsPanel;

    private void OnEnable()
    {
        SoundManager.Instance.PlaySFX("Pause");
        NarrativeCore.Instance.IsPaused = true;
    }

    private void OnDisable()
    {
        NarrativeCore.Instance.IsPaused = false;
        NarrativeCore.Instance.ReducingEnergy();
    }

    public void ContinueGame()
    {
        gameObject.SetActive(false);
    }

    public void MuteGame()
    {
        SoundManager.Instance.MuteGame();
    }

    public void OpenCredits()
    {
        CreditsPanel.SetActive(true);
        gameObject.SetActive(false);
    }

    public void QuitGame()
    {
        SceneManager.LoadScene("Intro");
    }
}
