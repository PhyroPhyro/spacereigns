﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBarView : MonoBehaviour {

    public Image FrontImg;
    public Image BackRedImg;
    public Image BackYellowImg;

    private Vector2 maxSize;

    private void Start()
    {
        maxSize = FrontImg.rectTransform.sizeDelta;
    }

    public void SetNewPercentage(float newPerc, bool playSound, Action setPercReturn = null)
    {
        if((maxSize.y * newPerc) < BackRedImg.rectTransform.sizeDelta.y)
        {
            if(playSound) SoundManager.Instance.PlaySFX("LoseEnergy");
            FrontImg.rectTransform.sizeDelta = new Vector2(maxSize.x, newPerc * maxSize.y);
            BackYellowImg.rectTransform.sizeDelta = new Vector2(maxSize.x, newPerc * maxSize.y);
            StartCoroutine(BackgroundAnimation(newPerc, BackRedImg, setPercReturn));
        }
        else
        {
            if (playSound) SoundManager.Instance.PlaySFX("GainEnergy");
            BackRedImg.rectTransform.sizeDelta = new Vector2(maxSize.x, newPerc * maxSize.y);
            BackYellowImg.rectTransform.sizeDelta = new Vector2(maxSize.x, newPerc * maxSize.y);
            StartCoroutine(BackgroundAnimation(newPerc, FrontImg, setPercReturn));
        }

    }

    IEnumerator BackgroundAnimation(float newPerc, Image image, Action animCallback = null)
    {
        yield return new WaitForSeconds(1f);

        float elapsedTime = 0;
        Vector2 previousSize = image.rectTransform.sizeDelta;
        Vector2 newSize = new Vector2(1, newPerc * maxSize.y);

        while (elapsedTime < 0.3f)
        {
            image.rectTransform.sizeDelta = Vector2.Lerp(previousSize, new Vector2(maxSize.x, newSize.y), (elapsedTime / 0.3f));
            elapsedTime += Time.fixedDeltaTime;
            yield return new WaitForEndOfFrame();
        }

        image.rectTransform.sizeDelta = new Vector2(previousSize.x, newSize.y);

        if (animCallback != null)
            animCallback();
    }
}
