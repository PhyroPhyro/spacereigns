﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using GooglePlayGames;

public class IntroView : MonoBehaviour {

    public Image OverlayImage;
    public GameObject IntroFrame;
    public Button TouchSkip;
    public Button SoundBtn, MutedBtn;
    public Text IntroText;
    public GameObject Arrow;
    [TextArea]
    public string PrologueText;

    private bool canSkip;
    private bool completedText;
    private Coroutine prologueRoutine;
    private float openingTicks;
    private float openingSeconds;
    private int startGameCount;

    private void Start()
    {
        SoundManager.Instance.PlayBGM("MainMenu");

        //Services
        startGameCount = PlayerPrefs.GetInt(GameConstants.START_GAME_COUNT, 0);
        Advertisement.Initialize(GameConstants.GOOGLE_PLAY_UNITY_ADS, GameConstants.ADS_TEST_MODE);
    }

    private void FixedUpdate()
    {
        openingTicks += Time.fixedDeltaTime;
        openingSeconds = openingTicks % 60;
    }

    public void StartIntro()
    {
        SoundManager.Instance.PlaySFX("StartGame");

        //Services
        openingTicks = 0;
        startGameCount++;
        PlayerPrefs.SetInt(GameConstants.START_GAME_COUNT, startGameCount);

        OverlayImage.gameObject.SetActive(true);
        StartCoroutine(BackgroundAnimation(1f, OverlayImage, 2f, 0f));
        StartCoroutine(BackgroundAnimation(0f, OverlayImage, 2f, 2f, () => { OverlayImage.gameObject.SetActive(false); }));
        StartCoroutine(ActivateOnDelay(2f, IntroFrame));
        
        prologueRoutine = StartCoroutine(StaticMethods.TextWriter(IntroText, PrologueText, () => {
            Arrow.SetActive(true);
            completedText = true;
        }, 3f));
        StartCoroutine(StartCounterToSkip());

        TouchSkip.gameObject.SetActive(true);
    }

    private void SendOpeningAnalytics()
    {
        AnalyticsEvent.Custom(GameConstants.OPENING_ANALYTICS_KEY, new Dictionary<string, object>
        {
            { GameConstants.TIME_TO_SKIP_OPENING, openingSeconds},
            { GameConstants.START_GAME_COUNT , startGameCount}
        });
    }

    IEnumerator ActivateOnDelay(float delay, GameObject obj)
    {
        yield return new WaitForSeconds (delay);
        obj.SetActive(true);
    }

    IEnumerator StartCounterToSkip()
    {
        yield return new WaitForSeconds(3f);
        canSkip = true;
    }

    IEnumerator BackgroundAnimation(float newAlpha, Image image, float time, float delay, Action returnAction = null)
    {
        yield return new WaitForSeconds(delay);

        image.gameObject.SetActive(true);

        float elapsedTime = 0;
        float prevAlpha = image.color.a;

        while (elapsedTime < time)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, Mathf.Lerp(prevAlpha, newAlpha, (elapsedTime / time)));
            elapsedTime += Time.fixedDeltaTime;
            yield return new WaitForEndOfFrame();
        }

        image.color = new Color(image.color.r, image.color.g, image.color.b, newAlpha);

        if (returnAction != null)
            returnAction();
    }

    public void ClickSkip()
    {
        if (completedText)
        {
            SendOpeningAnalytics();
            SceneManager.LoadScene("Game");
        }

        if (canSkip)
        {
            completedText = true;
            IntroText.text = PrologueText;
            StopCoroutine(prologueRoutine);
            Arrow.SetActive(true);
        }
    }

    public void ToggleSound()
    {
        SoundManager.Instance.MuteGame();
        SoundBtn.gameObject.SetActive(!SoundManager.Instance.IsMuted);
        MutedBtn.gameObject.SetActive(SoundManager.Instance.IsMuted);
    }

    public void ShowAchievements()
    {
        // show achievements UI
        if(PlayGamesPlatform.Instance.IsAuthenticated())
        {
            PlayGamesPlatform.Instance.ShowAchievementsUI();
        }
        else
        {
            GooglePlayManager.Instance.TryToInitPlayGames(() => {
                PlayGamesPlatform.Instance.ShowAchievementsUI();
            });
        }
    }
}
