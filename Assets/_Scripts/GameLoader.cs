﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GameLoader : MonoBehaviour
{
    public NarrativeCore DataCore;
    public TextAsset DialogSheet; 
    public TextAsset TraitSheet; 
    public TextAsset ChoiceSheet;
    public TextAsset EndingSheet;

    private void Start()
    {
        SoundManager.Instance.PlayBGM("Gameplay");
        SoundManager.Instance.PlayAmbience("Ship");

        DataCore.CoreTraits = new Dictionary<int, NarrativeTrait>();
        string[] traitSplit = TraitSheet.text.Split('\n');
        string[] traitProperties = GetSplit(traitSplit[0], '\t');
        for (int i = 1; i < traitSplit.Length; i++)
        {
            string[] currentTrait = GetSplit(traitSplit[i], '\t');

            NarrativeTrait trait = new NarrativeTrait();

            trait.TraitId = int.Parse(currentTrait[GetIndexOf(traitProperties, "Id")]);
            trait.TraitName = currentTrait[GetIndexOf(traitProperties, "Name")];
            trait.TraitValue = int.Parse(currentTrait[GetIndexOf(traitProperties, "StartingValue")]);

            DataCore.CoreTraits.Add(trait.TraitId, trait);
        }

        DataCore.CoreChoices = new Dictionary<int, NarrativeChoice>();
        string[] choiceSplit = ChoiceSheet.text.Split('\n');
        string[] choiceProperties = GetSplit(choiceSplit[0],'\t');
        for (int i = 1; i < choiceSplit.Length; i++)
        {
            string[] currentChoice = GetSplit(choiceSplit[i],'\t');

            NarrativeChoice choice = new NarrativeChoice();
            choice.ChoiceId = int.Parse(currentChoice[GetIndexOf(choiceProperties, "Id")]);
            choice.ChoiceTitle = currentChoice[GetIndexOf(choiceProperties, "Title")];
            choice.ChoiceSpeak = currentChoice[GetIndexOf(choiceProperties, "Speak")];
            choice.ChoiceEndingAnimation = int.Parse(currentChoice[GetIndexOf(choiceProperties, "ExitAnimation")]);

            choice.ChoiceTraitModifiers = new Dictionary<NarrativeTrait, int>();
            string[] choiceTraitsIds = currentChoice[GetIndexOf(choiceProperties, "TraitModifier")].Split('/');
            for (int j = 0; j < choiceTraitsIds.Length; j++)
            {
                string[] choiceTraitsIdsSplit = choiceTraitsIds[j].Split(':');
                choice.ChoiceTraitModifiers.Add(DataCore.CoreTraits[int.Parse(choiceTraitsIdsSplit[0])], int.Parse(choiceTraitsIdsSplit[1]));
            }

            DataCore.CoreChoices.Add(choice.ChoiceId, choice);
        }

        DataCore.CoreDialog = new Dictionary<int, NarrativeDialog>();
        string[] dialogSplit = DialogSheet.text.Split('\n');
        string[] dialogProprieties = GetSplit(dialogSplit[0],'\t');

        for (int i = 1; i < dialogSplit.Length; i++)
        {
            string[] currentDialog = GetSplit(dialogSplit[i],'\t');

            NarrativeDialog dialog = new NarrativeDialog();

            dialog.DialogId = int.Parse(currentDialog[GetIndexOf(dialogProprieties, "Id")]);
            dialog.DialogTitle = currentDialog[GetIndexOf(dialogProprieties, "Title")];
            dialog.DialogEnterAnimationId = int.Parse(currentDialog[GetIndexOf(dialogProprieties, "EnterAnimation")]);
            dialog.DialogCharacterId = int.Parse(currentDialog[GetIndexOf(dialogProprieties, "Character")]);

            dialog.DialogChoices = new List<NarrativeChoice>();
            string[] dialogChoicesIds = currentDialog[GetIndexOf(dialogProprieties, "Choices")].Split('/');
            for (int j = 0; j < dialogChoicesIds.Length; j++)
            {
                dialog.DialogChoices.Add(DataCore.CoreChoices[int.Parse(dialogChoicesIds[j])]);
            }

            dialog.DialogNecessaryTraits = new Dictionary<NarrativeTrait, int>();
            string[] dialogTraitsIds = currentDialog[GetIndexOf(dialogProprieties, "Traits")].Split('/');
            for (int j = 0; j < dialogTraitsIds.Length; j++)
            {
                string[] dialogTraitsIdsSplit = dialogTraitsIds[j].Split(':');
                dialog.DialogNecessaryTraits.Add(DataCore.CoreTraits[int.Parse(dialogTraitsIdsSplit[0])], int.Parse(dialogTraitsIdsSplit[1]));
            }

            dialog.DialogNecessaryChoices = new List<NarrativeChoice>();
            if (currentDialog.Length > 5)
            {
                string[] dialogNecessaryChoicesIds = currentDialog[GetIndexOf(dialogProprieties, "NecessaryChoices")].Split('/');
                for (int j = 0; j < dialogNecessaryChoicesIds.Length; j++)
                {
                    if(!string.IsNullOrEmpty(dialogNecessaryChoicesIds[0]))
                        dialog.DialogNecessaryChoices.Add(DataCore.CoreChoices[int.Parse(dialogNecessaryChoicesIds[j])]);
                }
            }

            DataCore.CoreDialog.Add(dialog.DialogId, dialog);
        }

        DataCore.CoreEndings = new Dictionary<int, NarrativeEnding>();
        string[] endingsSplit = EndingSheet.text.Split('\n');
        string[] endingProperties = GetSplit(endingsSplit[0],'\t');

        for (int i = 1; i < endingsSplit.Length; i++)
        {
            string[] currentEnding = GetSplit(endingsSplit[i],'\t');

            NarrativeEnding ending = new NarrativeEnding();
            ending.EndingId = int.Parse(currentEnding[GetIndexOf(endingProperties, "Id")]);
            ending.EndingTitle = currentEnding[GetIndexOf(endingProperties, "Title")];
            ending.EndingPicture = int.Parse(currentEnding[GetIndexOf(endingProperties, "Picture")]);
            ending.EndingTurns = int.Parse(currentEnding[GetIndexOf(endingProperties, "Turns")]);
            ending.IsGoodEnding = bool.Parse(currentEnding[GetIndexOf(endingProperties, "IsGoodEnding")]);

            ending.EndingNecessaryTraits = new Dictionary<NarrativeTrait, int>();
            string[] endingTraitsIds = currentEnding[GetIndexOf(endingProperties, "Traits")].Split('/');
            for (int j = 0; j < endingTraitsIds.Length; j++)
            {
                if (!string.IsNullOrEmpty(endingTraitsIds[j]))
                {
                    string[] endingTraitsIdsSplit = endingTraitsIds[j].Split(':');
                    ending.EndingNecessaryTraits.Add(DataCore.CoreTraits[int.Parse(endingTraitsIdsSplit[0])], int.Parse(endingTraitsIdsSplit[1]));
                }
            }

            ending.EndingNecessaryChoices = new List<NarrativeChoice>();
            if (GetIndexOf(endingProperties, "NecessaryChoices") != -1)
            {
                string[] endingNecessaryChoicesIds = currentEnding[GetIndexOf(endingProperties, "NecessaryChoices")].Split('/');
                for (int j = 0; j < endingNecessaryChoicesIds.Length; j++)
                {
                    if(!string.IsNullOrEmpty(endingNecessaryChoicesIds[j]))
                        ending.EndingNecessaryChoices.Add(DataCore.CoreChoices[int.Parse(endingNecessaryChoicesIds[j])]);
                }
            }

            DataCore.CoreEndings.Add(ending.EndingId, ending);
        }

        StartGame();
    }

    private void StartGame()
    {
        NarrativeCore.Instance.StartNarrative();
    }

    private int GetIndexOf(string[] array, string obj)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = array[i].Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
        }
        return Array.IndexOf(array, obj);
    }

    private string[] GetSplit(string sentence, char separator)
    {
        string[] split = sentence.Split(separator);

        for (int i = 0; i < split.Length; i++)
        {
            split[i] = split[i].Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
        }

        return split;
    }
}
